#include <stdio.h>
#include <stdlib.h>
#include "lista.h"

typedef struct Item{
    float value;
    struct Item * prox;
} item;

item * criar_item(float value){
    item * novo = (item *)malloc(sizeof(item));
    if (!novo){
        printf("Not enought memory\n");
        exit(1);
    }
    novo->value = value;
    novo->prox = NULL;
    return novo;
}

void append(item * lista, item * novo){
    novo->prox = lista->prox;
    lista->prox = novo;
}

void push(item * lista, item * novo){
    item * t = lista;
    while (t->prox != NULL){
        t = t->prox;
    }
    append(t,novo);
}

item * insert(item * lista, item * novo, int id){
    // Verificação
    if (id > len(lista)){
        printf("Index out of range\n");
        exit(1);
    }
    item * t = lista, * save = lista;
    int i;
    for (i = 0; i < id; i++){
        save = t;
        t = t->prox;
    }
    if (i == 0){
        novo->prox = lista;
        return novo;
    }
    save->prox = novo;
    novo->prox = t;
    return lista;

}

void imprimir_lista(item * lista){
    item * t = lista;
    printf("[");
    for (t; t != NULL; t = t->prox){
        printf("%f", t->value);
        if (t->prox != NULL){
            printf(" ");
        }
    }
    printf("]\n");
}

void excluir_lista(item * lista){
    item * t = lista, * save;
    while (t != NULL){
        save = t->prox;
        free(t);
        t = save;
    }
}

void pop(item * lista, item * excluido){
    item * t = excluido->prox;
    free(excluido);
    lista->prox = t;
}

void lastpop(item * lista){
    item * t = lista, * save;
    while (t->prox != NULL){
        save = t;
        t = t->prox;
    }
    pop(save, t);
}

item * remove_all(item * lista, float param){
    item * t = lista, *save, *temp = lista;

    // Função funcionando apenas para o caso que o primeiro elemento não é o procurado
    /*if (t->value == param){
        t = shift(lista);
        while (t->value == param){
            temp = t;
            t = t->prox;
            shift(temp);
        }
        
    }*/

    while (t != NULL){
        if (t->value == param){
            temp = t;
            t = t->prox;
            pop(save, temp);

        }else{
            save = t;
            t = t->prox;
        }
        
    }
    return lista;
}

item * fill(item * lista, item * novo, int idx){
    item * t = lista, *save;
    int i;
    for (i = 0; i < idx; i++){
        save = t;
        t = t->prox;
    }

    append(t, criar_item(novo->value));
    append(save, criar_item(novo->value));
    return lista;

}

int search(item * lista, float procurado){
    int id = 0;
    item * t = lista;
    while (t != NULL){
        if (t->value == procurado){
            return id;
        }
        id++;
        t = t->prox;
    }
    return -1;
}

int len(item * lista){
    item * t = lista;
    int len = 0;
    while (t != NULL){
        len++;
        t = t->prox;
    }
    return len;
}

float encontrar(item * lista, int id){
    item * t = lista;
    for (int i = 0; i < id; i++){
        t = t->prox;
    }
    return t->value;
}

item * shift(item * lista){
    item * t = lista;
    t = t->prox;
    free(lista);
    return t;
}

item * partir(item * ele1, item * ele2){
    ele1->prox = NULL;
    return ele2;
}

item * dividir2(item * lista){
    int meio = len(lista) / 2, aux = 0;
    item * t = lista, * save = lista;
    while (aux < meio){
        save = t;
        t = t->prox;
        aux++;
    }
    // Quebrar ligação da lista nesse ponto
    return partir(save, t);
}

item * extend(item * lista1, item * lista2){
    item * t = lista1;
    while (t->prox != NULL){
        t = t->prox;
    }
    t->prox = lista2;
    return lista1;
}

item * vet_to_cad(float vet[], int len){
    item * lista = criar_item(vet[0]);
    for (int i = 1; i < len; i++){
        push(lista, criar_item(vet[i]));
    }
    return lista;
}