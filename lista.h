
// Tipo exportado
typedef struct Item item;

// Função de criar item da lista
item * criar_item(float value);

// Função de adicionar um item ao início lista
void append(item * lista, item * novo);

// Função de adicionar um item ao final da lista
void push(item * lista, item * novo);

// Função para adicionar um item em uma certa posição da lista encadeada
item * insert(item * lista, item * novo, int id);

// Função de imprimir lista
void imprimir_lista(item * lista);

// Função de liberar lista
void excluir_lista(item * lista);

// Função de excluir um item da lista
void pop(item * lista, item * excluido);

// Função para excluir o último elemento da lista
void lastpop(item * lista);

// Função para excluir o primeiro elemento da lista, retorna o novo elemento que inicia a lista
item * shift(item * lista);

// Função que remove todas as aparições de um determinado parametro da lista
item * remove_all(item * lista, float param);

item * fill(item * lista, item * novo, int id);

// Procura por um elemento na lista e retorna o índice da sua primeira aparição
int search(item * lista, float procurado);

// Retorna o tamanho da lista encadeada
int len(item * lista);

// Retorna o valor no índice dado da lista
float encontrar(item * lista, int id);

// Função para dividir a lista em 2. Retorna o elemento inicial da segunda lista, o primeiro continua sendo o primeiro da primeira lista
item * dividir2(item * lista);

// Função auxiliar: Quebra a ligação entre dois elementos da lista, retorna um ponteiro para o segundo elemento (O que inicia a segunda lista);
item * partir(item * ele1, item * ele2);

// Função para extender uma lista a partir de outro (Junta duas listas em uma só)
item * extend(item * lista1, item * lista2);

// Função que recebe um vetor e seu tamanho e retorna uma lista encadeada com os mesmos elementos do vetor
item * vet_to_cad(float vet[], int len);
